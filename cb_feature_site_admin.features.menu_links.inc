<?php
/**
 * @file
 * cb_feature_site_admin.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function cb_feature_site_admin_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-admin-menu_content:admin/content.
  $menu_links['menu-site-admin-menu_content:admin/content'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'link_path' => 'admin/content',
    'router_path' => 'admin/content',
    'link_title' => 'content',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-admin-menu_content:admin/content',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('content');

  return $menu_links;
}
