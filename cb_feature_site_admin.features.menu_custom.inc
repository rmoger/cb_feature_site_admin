<?php
/**
 * @file
 * cb_feature_site_admin.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function cb_feature_site_admin_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-site-admin-menu.
  $menus['menu-site-admin-menu'] = array(
    'menu_name' => 'menu-site-admin-menu',
    'title' => 'Site Admin Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Site Admin Menu');

  return $menus;
}
